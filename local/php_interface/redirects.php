<?
$redirect = array(
	"/dostavka" => "/delivery/",
	"/kachestvo" => "/quality/",
	"/kontakti" => "/contacts/",
	"/obmen_i_vozvrat" => "/vozvrat/",
	"/ptica" => "/catalog/ptica/",
	"/ptica/indeyka" => "/catalog/ptica/indeyka/",
	"/ptica/kurica" => "/catalog/ptica/kurica/",
	"/ptica/utka" => "/catalog/ptica/utka/",
	"/kulinariya_i_shashlik/kolbaski" => "/catalog/gotovaya-produktsiya/kolbaski/",
	"/kulinariya_i_shashlik/kotleti" => "/catalog/gotovaya-produktsiya/kotleti/",
	"/kulinariya_i_shashlik" => "/catalog/gotovaya-produktsiya/shaslik/",
	"/kulinariya_i_shashlik/kulinariya" => "/catalog/gotovaya-produktsiya/shaslik/",
	"/myaso" => "/catalog/myaso/",
	"/myaso/govyadina_halyal" => "/catalog/khalyal/govyadina_halyal/",
	"/myaso/myasnie_nabori" => "/catalog/myaso/myasnie_nabori/",
	"/myaso/svinina" => "/catalog/myaso/svinina/",
	"/myaso/baranina" => "/catalog/myaso/baranina/",
	"/myaso/baranina_halyal" => "/catalog/khalyal/baranina_halyal/",
	"/myaso/govyadina" => "/catalog/myaso/govyadina/",
	"/krolik" => "/catalog/myaso/krolik/",
	"/farshi_i_subprodukti/subprodukti" => "/catalog/farshi_i_subprodukti/subprodukti/",
	"/farshi_i_subprodukti" => "/catalog/farshi_i_subprodukti/farshi/",
);

if(isset($redirect[$_SERVER['REQUEST_URI']]))
{
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: ".$redirect[$_SERVER['REQUEST_URI']]);
    die;
}