<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

require_once 'libs/Mobile_Detect.php';
$detect = new Mobile_Detect;

// utm-метки в куки
foreach($_REQUEST as $key => $val) {
	if(preg_match('/utm_/',$key)) {
		setcookie($key, $val, time()+3600, "/");
	}
}
if(!preg_match('/meat4all\.ru/', $_SERVER['HTTP_REFERER'])) {
    $tmp = explode('?', $_SERVER['HTTP_REFERER']);
    setcookie('referer', $tmp[0], time()+3600, "/");
}
$massUTM = array('utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term');    
foreach($massUTM as $val) {     
	$FORM_UTM .= "\n".$val.': '.$_COOKIE[$val];       
}
if(isset($_COOKIE['referer']) && $_COOKIE['referer'] != '') {
	$FORM_UTM .= "\nНа сайт перешли с внешнего ресурса - " . $_COOKIE['referer'];
}
else {
	$FORM_UTM .= "\nНе осуществлялось перехода с внешнего ресурса.";
}
//

// редиректы со старого сайта
if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/redirects.php")){
	include($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/redirects.php");
}

// Стоимость бесплатной доставки
function getFreeDelivery() {
	if( CModule::IncludeModule("iblock") ){
		$db_props = CIBlockElement::GetProperty(10, 2374, array(), Array("CODE"=>"FREE_DELIVERY"));
		if($ar_props = $db_props->Fetch())
			return IntVal($ar_props["VALUE"]);
		else
			return false;
	}
}

AddEventHandler('form', 'onBeforeResultAdd', 'onBeforeResultAddHandler');
AddEventHandler('form', 'onAfterResultAdd', 'onAfterResultAddHandler');

// Добавляем список товаров из корзины в сообщение
function onBeforeResultAddHandler($WEB_FORM_ID, &$arFields, &$arrVALUES){
  
  if ($WEB_FORM_ID == 3) { // форма заказа
	
		// получаем массив емейлов из результатов формы
		/*
		CModule::IncludeModule("form");
		CForm::GetResultAnswerArray(
			3, //ORDER form
			$arrColumns, 
			$arrAnswers, 
			$arrAnswersVarname, 
			array()
		);
		$arOrderEmails = array();
		foreach ($arrAnswersVarname as $arAnswer){
			if ( isset($arAnswer["EMAIL"]) ){
				$arOrderEmails[] = $arAnswer["EMAIL"][0]["USER_TEXT"];
			}
		}
		$arOrderEmails = array_unique($arOrderEmails);
		//
		 $arrVALUES['form_hidden_22'] = "";
		if ( !in_array($arrVALUES["form_email_21"], $arOrderEmails) ){
			$arrVALUES['form_hidden_22'] = "<br><span style='border:1px solid #bd1726; color:#bd1725; padding:5px 10px; font-size:120%;'>Это новый покупатель, сюда нужно положить подарок.</span><br>";
		}
		*/
		
		// определяем нового пользователя по номеру телефона и добавляем строку NEW в письмо
		
		\Bitrix\Main\Loader::includeModule("form");

		$arFieldsForm[] = array(
		   "CODE"              => "PHONE",
		   "FILTER_TYPE"       => "text",
		   "PARAMETER_NAME"    => "USER",
		   "VALUE"             => $arrVALUES["form_text_8"],
		   "EXACT_MATCH"       => "N"
		);
		$arFilterForm = array (
		   "FIELDS" => $arFieldsForm,  
		);
		$rsResults = CFormResult::GetList(3, $by = 's_id', $order = 'asc', $arFilterForm, $is_filtered, 'N', false);

		if (!$arResultOb = $rsResults->Fetch())
		{
			$arrVALUES['form_hidden_22'] = "<span style='color:#bd1725; font-weight:bold;'>NEW</span>,<span style='padding-left:3px;'>&nbsp;</span>";
		}
				
		foreach ($_SESSION["CATALOG_USER_COUPONS"] as $USER_COUPONS){
		 	if ( $USER_COUPONS == "ЛЮБЛЮ" || $USER_COUPONS == "Скидка150"){
		 		$arrVALUES['form_hidden_23'] = $USER_COUPONS;
		 	}
		}
	
		CModule::IncludeModule("sale");
		$arBasketItems = array();
		 $dbBasketItems = CSaleBasket::GetList(
			array("NAME" => "ASC", "ID" => "ASC"),
			array(
				"FUSER_ID" => CSaleBasket::GetBasketUserID(),
				"LID" => SITE_ID,
				"ORDER_ID" => "NULL",
				"CAN_BUY" => "Y"
			),
			false,
			false,
			array("ID", "PRODUCT_ID", "QUANTITY", "NAME", "PRICE", "MEASURE_NAME", "CURRENCY", "CAN_BUY", "DETAIL_PAGE_URL")
		); 
		while ($arItems = $dbBasketItems->Fetch()){
			$arBasketItems[] = $arItems;
		}
		
		// получаем цены с учетом скидки
		$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), SITE_ID);
		$basket->refreshData(array('PRICE', 'COUPONS'));
		$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true)));
		$discounts->calculate();
		$result = $discounts->getApplyResult(true);
		$arPrices = $result['PRICES']['BASKET'];

		$products = '';
		$total = 0;
		$sum = 0;
		
		if ($arBasketItems){
			foreach ($arBasketItems as $key => $item){

				// Получаем id товара по id торгового предложения и изображение товара
				
				$productID = '';
				$offerInfo = array();
				$productImage = '';
				
				$offerInfo = CCatalogSku::GetProductInfo($item["PRODUCT_ID"]);				
				
				if(is_array($offerInfo))
				{
					$productID = $offerInfo["ID"];
				} else {
					$productID = $item["PRODUCT_ID"];
				}
				
				$res = CIBlockElement::GetByID($productID);
				
				if($arRes = $res->GetNext()){
					$file = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$productImage = '<img src="https://'.$_SERVER["SERVER_NAME"].$file['src'].'" alt="nav" border="0" width="50" style="display:block; margin:0;">';
				}

				// Получаем свойство "Вес" элемента корзины с кодом $basketID
				$basketItemWeight = array();
				$weight = '';
				$dbBasketProps = CSaleBasket::GetPropsList(
					array("SORT" => "ASC"), array("BASKET_ID" => $item["ID"])
				);
				while ($arBasketProps = $dbBasketProps->Fetch())
				{
					if ($arBasketProps["CODE"]=="WEIGHT"){
					 $basketItemWeight = $arBasketProps["VALUE"];
					} 
				}
				if (!empty($basketItemWeight)){					
					$weight = '<p style="margin-top: 2px; margin-bottom: 8px;">Желаемый вес: '.$basketItemWeight.'</p>';
				}				
								
				$sum = $arPrices[$item["ID"]]["PRICE"]*$item["QUANTITY"];
				$total += $sum;
				
				$products .= '<tr>
					<td style="padding-top: 40px; border-bottom: 1px solid #c6c6c6;">
						<table border="0" cellpadding="0" cellspacing="0" style="font-size: 14px; height:auto;margin:0 auto;padding:0; color: #1e1e1e;" width="520">
							<tr>
								<td style="font-weight: bold;" valign="top">
									'.($key + 1).'
								</td>
								<td valign="top" style="width: 80px;" align="center">
									'.$productImage.'									
								</td>
								<td valign="top">
									<p style="font-weight: bold; margin-top: 0px; margin-bottom: 8px;"><a style="text-decoration: none; color: #000;" href="https://'.$_SERVER["SERVER_NAME"].$item["DETAIL_PAGE_URL"].'">'.$item["NAME"].'</a></p>
									'.$weight.'									
									<p style="margin-top: 4px; margin-bottom: 24px;">Итоговая стоиомсть будет определена <br/> после согласования с оператором, <br/>либо мы свяжемся сами для уточнения <br/>деталей заказа.</p>
								</td>
								<td valign="top" style="width: 50px; text-align: left;">
									'.$item["QUANTITY"].' '.$item["MEASURE_NAME"].'
								</td>
								<td valign="top" style="text-align: right; width: 80px;">'.$sum.' р.</td>
							</tr>
						</table>
					</td>
				</tr>';
			}
			
			$products .=
			'<tr>
				<td style="text-align: right; font-weight: 600; font-size: 16px; padding-top: 26px;">
					Сумма: '.$total.' р.
				</td>
			</tr>';
			
			$arrVALUES['form_textarea_12'] = $products;
			$arrVALUES['form_hidden_24'] = $total;
		}		
  }
}

// Очищаем корзину после добавления результата формы
function onAfterResultAddHandler($WEB_FORM_ID, $RESULT_ID){
  if ($WEB_FORM_ID == 3) { // форма заказа
		CModule::IncludeModule("sale");
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
  }
}


// пользовательские поля для разделов
function GetSectionField($iblock_id, $section_id, $uf_id) {
	$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ("IBLOCK_".$iblock_id."_SECTION", $section_id);
	return $arUF[$uf_id]["VALUE"];
}

// отложенная функция для вывода h1
function ShowH1(){
  global $APPLICATION;
	$hide_h1 = $APPLICATION->GetPageProperty("hide-h1");
	
	if ( $hide_h1=="Y" ){
    return false;
  } else {
		$h1 = $APPLICATION->GetPageProperty("h1");
		if( empty($h1) ){
			$h1 = $APPLICATION->GetTitle(false);
		}
		return "<h1>".$h1."</h1>";
	}
}





?>