<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!$isIndex){?>
	</div><!-- /.container-content -->
<?}?>

<div class="scroll-top"> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"> <g> <path style="fill:#b0b0b0;" d="M24,3.125c11.511,0,20.875,9.364,20.875,20.875S35.511,44.875,24,44.875S3.125,35.511,3.125,24S12.489,3.125,24,3.125 M24,0.125C10.814,0.125,0.125,10.814,0.125,24S10.814,47.875,24,47.875S47.875,37.186,47.875,24S37.186,0.125,24,0.125L24,0.125z"></path> </g> <path style="fill:#b0b0b0;" d="M25.5,36.033c0,0.828-0.671,1.5-1.5,1.5s-1.5-0.672-1.5-1.5V16.87l-7.028,7.061c-0.293,0.294-0.678,0.442-1.063,0.442 c-0.383,0-0.766-0.146-1.058-0.437c-0.587-0.584-0.589-1.534-0.005-2.121l9.591-9.637c0.281-0.283,0.664-0.442,1.063-0.442 c0,0,0.001,0,0.001,0c0.399,0,0.783,0.16,1.063,0.443l9.562,9.637c0.584,0.588,0.58,1.538-0.008,2.122 c-0.589,0.583-1.538,0.58-2.121-0.008l-6.994-7.049L25.5,36.033z"></path> </svg> </div>

<!--a class="fixed-callback-btn" href="javascript:;" onclick="yaCounter50565871.reachGoal('vopros'); return true;">
	<div class="animated-circle"></div>
	<svg class="callback__icon" width="28px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.6 17.5"><path d="M3.7 17.5v-4.8H2c-1.1 0-2-.9-2-2V2C0 .9.9 0 2 0h14.6c1.1 0 2 .9 2 2v8.8c0 1.1-.9 2-2 2H9l-5.3 4.7zM2 1c-.6 0-1 .4-1 1v8.8c0 .5.4 1 1 1h2.8v3.5l3.8-3.5h8c.5 0 1-.4 1-1V2c0-.5-.4-1-1-1H2z"></path></svg>
	<svg class="callback__icon-close" width="16px" height="16px" viewBox="0 0 23 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="#000" fill-rule="evenodd"> <rect transform="translate(11.313708, 11.313708) rotate(-45.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect> <rect transform="translate(11.313708, 11.313708) rotate(-315.000000) translate(-11.313708, -11.313708) " x="10.3137085" y="-3.6862915" width="2" height="30"></rect> </g> </svg>
</a-->

<div class="fixed-callback-form">
	<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "fixed", Array(
			"WEB_FORM_ID" => "2",	// ID веб-формы
			"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
			"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
			"SEF_MODE" => "N",	// Включить поддержку ЧПУ
			"SEF_FOLDER" => "/",	// Каталог ЧПУ (относительно корня сайта)
			"CACHE_TYPE" => "N",	// Тип кеширования
			"CACHE_TIME" => "0",	// Время кеширования (сек.)
			"AJAX_MODE" => "Y",
			"AJAX_OPTION_STYLE" => "Y",
			"LIST_URL" => "",	// Страница со списком результатов
			"EDIT_URL" => "",	// Страница редактирования результата
			"SUCCESS_URL" => $_SERVER["REQUEST_URI"],	// Страница с сообщением об успешной отправке
			"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
			"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
			"VARIABLE_ALIASES" => array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID",
			)
		),
		false
	);?>
</div>

<div class="form-popup" id="callback">
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "popup", Array(
		"WEB_FORM_ID" => "1",	// ID веб-формы
		"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"SEF_FOLDER" => "/",	// Каталог ЧПУ (относительно корня сайта)
		"CACHE_TYPE" => "N",	// Тип кеширования
		"CACHE_TIME" => "0",	// Время кеширования (сек.)
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"LIST_URL" => "",	// Страница со списком результатов
		"EDIT_URL" => "",	// Страница редактирования результата
		"SUCCESS_URL" => $_SERVER["REQUEST_URI"],	// Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
</div>

<?/*
<div class="form-popup form-moving" id="moving">
	<div class="form-moving-title">На данный момент наш сайт переезжает на другую платформу.</div>
	<div class="form-moving-description">
	В связи с этим могут возникнуть ошибки при оформлении заказа.<br> 
	Пожалуйста, если вы заметили ошибку или столкнулись с проблемой,<br> 
	позвоните нам по тел. <span class="mgo-number-15282">+7 (495) 150-98-46</span> или напишите нам.
	</div>
	<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "moving", Array(
			"WEB_FORM_ID" => "4",	// ID веб-формы
			"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
			"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
			"SEF_MODE" => "N",	// Включить поддержку ЧПУ
			"SEF_FOLDER" => "/",	// Каталог ЧПУ (относительно корня сайта)
			"CACHE_TYPE" => "N",	// Тип кеширования
			"CACHE_TIME" => "0",	// Время кеширования (сек.)
			"AJAX_MODE" => "Y",
			"AJAX_OPTION_STYLE" => "Y",
			"LIST_URL" => "",	// Страница со списком результатов
			"EDIT_URL" => "",	// Страница редактирования результата
			"SUCCESS_URL" => $_SERVER["REQUEST_URI"],	// Страница с сообщением об успешной отправке
			"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
			"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
			"VARIABLE_ALIASES" => array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID",
			)
		),
		false
	);?>
</div>
*/?>
	
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 footer-col">
				<a href="/" class="logo-footer"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo-footer.png"></a>
				<div class="footer-socials">
					<a href="https://vk.com/meat4all" target="_blank" rel="nofollow"> <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="26px" height="26px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><desc>VK</desc><path style="fill:#ffffff;" d="M47.761,24c0,13.121-10.639,23.76-23.76,23.76C10.878,47.76,0.239,37.121,0.239,24c0-13.123,10.639-23.76,23.762-23.76C37.122,0.24,47.761,10.877,47.761,24 M35.259,28.999c-2.621-2.433-2.271-2.041,0.89-6.25c1.923-2.562,2.696-4.126,2.45-4.796c-0.227-0.639-1.64-0.469-1.64-0.469l-4.71,0.029c0,0-0.351-0.048-0.609,0.106c-0.249,0.151-0.414,0.505-0.414,0.505s-0.742,1.982-1.734,3.669c-2.094,3.559-2.935,3.747-3.277,3.524c-0.796-0.516-0.597-2.068-0.597-3.171c0-3.449,0.522-4.887-1.02-5.259c-0.511-0.124-0.887-0.205-2.195-0.219c-1.678-0.016-3.101,0.007-3.904,0.398c-0.536,0.263-0.949,0.847-0.697,0.88c0.31,0.041,1.016,0.192,1.388,0.699c0.484,0.656,0.464,2.131,0.464,2.131s0.282,4.056-0.646,4.561c-0.632,0.347-1.503-0.36-3.37-3.588c-0.958-1.652-1.68-3.481-1.68-3.481s-0.14-0.344-0.392-0.527c-0.299-0.222-0.722-0.298-0.722-0.298l-4.469,0.018c0,0-0.674-0.003-0.919,0.289c-0.219,0.259-0.018,0.752-0.018,0.752s3.499,8.104,7.573,12.23c3.638,3.784,7.764,3.36,7.764,3.36h1.867c0,0,0.566,0.113,0.854-0.189c0.265-0.288,0.256-0.646,0.256-0.646s-0.034-2.512,1.129-2.883c1.15-0.36,2.624,2.429,4.188,3.497c1.182,0.812,2.079,0.633,2.079,0.633l4.181-0.056c0,0,2.186-0.136,1.149-1.858C38.281,32.451,37.763,31.321,35.259,28.999"></path></svg> </a>
					
					<a href="https://www.instagram.com/meatforall/" target="_blank" rel="nofollow"> <svg class="t-sociallinks__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26px" height="26px" viewBox="0 0 30 30" xml:space="preserve"><desc>Instagram</desc><path style="fill:#ffffff;" d="M15,11.014 C12.801,11.014 11.015,12.797 11.015,15 C11.015,17.202 12.802,18.987 15,18.987 C17.199,18.987 18.987,17.202 18.987,15 C18.987,12.797 17.199,11.014 15,11.014 L15,11.014 Z M15,17.606 C13.556,17.606 12.393,16.439 12.393,15 C12.393,13.561 13.556,12.394 15,12.394 C16.429,12.394 17.607,13.561 17.607,15 C17.607,16.439 16.444,17.606 15,17.606 L15,17.606 Z"></path><path style="fill:#ffffff;" d="M19.385,9.556 C18.872,9.556 18.465,9.964 18.465,10.477 C18.465,10.989 18.872,11.396 19.385,11.396 C19.898,11.396 20.306,10.989 20.306,10.477 C20.306,9.964 19.897,9.556 19.385,9.556 L19.385,9.556 Z"></path><path style="fill:#ffffff;" d="M15.002,0.15 C6.798,0.15 0.149,6.797 0.149,15 C0.149,23.201 6.798,29.85 15.002,29.85 C23.201,29.85 29.852,23.202 29.852,15 C29.852,6.797 23.201,0.15 15.002,0.15 L15.002,0.15 Z M22.666,18.265 C22.666,20.688 20.687,22.666 18.25,22.666 L11.75,22.666 C9.312,22.666 7.333,20.687 7.333,18.28 L7.333,11.734 C7.333,9.312 9.311,7.334 11.75,7.334 L18.25,7.334 C20.688,7.334 22.666,9.312 22.666,11.734 L22.666,18.265 L22.666,18.265 Z"></path></svg> </a>
				</div>
				<div class="footer-copyright">
					<?=date('Y')?> <?$APPLICATION->IncludeFile(SITE_DIR."include/copyright.php", Array(), Array("MODE"=>"html") );?>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<div class="footer-menu-title">Наши товары</div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"footer", 
					array(
						"ROOT_MENU_TYPE" => "catalog",
						"CHILD_MENU_TYPE" => "",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"USE_EXT" => "Y",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "",
						"DELAY" => "N"
					),
					false
				);
				?>
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<div class="footer-menu-title">О нас</div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"footer", 
					array(
						"ROOT_MENU_TYPE" => "top",
						"CHILD_MENU_TYPE" => "",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"USE_EXT" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "",
						"DELAY" => "N"
					),
					false
				);
				?>
			</div>
			<div class="col-md-3 col-sm-6 footer-col">
				<div class="footer-menu-title">Информация</div>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"footer", 
					array(
						"ROOT_MENU_TYPE" => "bottom-info",
						"CHILD_MENU_TYPE" => "",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"USE_EXT" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "",
						"DELAY" => "N"
					),
					false
				);
				?>
			</div>
			
		</div>
	</div>
</footer>	

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript' data-skip-moving="true">
(function(){ var widget_id = 'YpNMSuibOi';var d=document;var w=window;function l(){
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
  s.src = '//code.jivosite.com/script/widget/'+widget_id
    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
  if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
  else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->
			

<!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(50565871, "init", {
        id:50565871,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50565871" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');	ga('create', 'UA-127111007-1', 'auto');	ga('send', 'pageview');	window.mainTracker = 'user';	if (! window.mainTracker) { window.mainTracker = 'tilda'; }	(function (d, w, k, o, g) { var n=d.getElementsByTagName(o)[0],s=d.createElement(o),f=function(){n.parentNode.insertBefore(s,n);}; s.type = "text/javascript"; s.async = true; s.key = k; s.id = "tildastatscript"; s.src=g; if (w.opera=="[object Opera]") {d.addEventListener("DOMContentLoaded", f, false);} else { f(); } })(document, window, '8761128b6ece309a633d027b1355cfe5','script','https://stat.tildacdn.com/js/tildastat-0.2.min.js');</script>
<script data-skip-moving="true">
    (function(w, d, u, i, o, s, p) {
        if (d.getElementById(i)) { return; } w['MangoObject'] = o; 
        w[o] = w[o] || function() { (w[o].q = w[o].q || []).push(arguments) }; w[o].u = u; w[o].t = 1 * new Date();
        s = d.createElement('script'); s.async = 1; s.id = i; s.src = u;
        p = d.getElementsByTagName('script')[0]; p.parentNode.insertBefore(s, p);
    }(window, document, '//widgets.mango-office.ru/widgets/mango.js', 'mango-js', 'mgo'));
    mgo({calltracking: {id: 15282, elements: [{"selector":".mgo-number-15282"}]}});
</script>
			

</body>
</html>

