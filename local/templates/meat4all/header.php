<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Page\Asset; 
use \Bitrix\Main\Context;

$context = Context::getCurrent();
$arRequest = $context->getRequest()->toArray();
$pagenavTitle = array_key_exists('PAGEN_1', $arRequest) ? ' - страница '.$arRequest['PAGEN_1'] : '';

$curPage = $APPLICATION->GetCurPage();
$isIndex = ($APPLICATION->GetCurPage() == '/');
$hideH1Bg = (($APPLICATION->GetDirProperty("HIDE_H1_BG") == "Y") ? true : false);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<title><?$APPLICATION->ShowTitle()?><?=$pagenavTitle?></title>
	<?//$APPLICATION->ShowHead();?>
	<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET?>" />
	<?$APPLICATION->ShowMeta("description")?>  
	<?$APPLICATION->ShowCSS()?>
	<?$APPLICATION->ShowHeadStrings()?>
	<?$APPLICATION->ShowHeadScripts()?>  
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
	<meta name="yandex-verification" content="57120a8393bb4245" />
	<meta name="google-site-verification" content="5q-Tr-0_IMsTgkA3-Az7Zj9wMPpLFLcgpPg0Q31Q4N4" />
	<?$APPLICATION->ShowViewContent('canonical');?>
	<?
	
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox.min.css"); 
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/js/slick/slick.css"); 
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/bootstrap-reboot.min.css"); 
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/bootstrap-grid.css"); 
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/css/styles.css"); 
	if ($curPage=="/contacts/"){
		Asset::getInstance()->addString('<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_7XM0V3HK4dB92V7kzHRT1e_R7557KWc&callback=initMap"></script>');
	}
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.1.11.3.min.js"); 
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/fancybox/jquery.fancybox.min.js"); 
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.min.js"); 
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/jquery.cookie.js"); 
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/slick/slick.min.js"); 
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/script.js"); 
	?>
	<!-- Facebook Pixel Code -->
	<script data-skip-moving="true">
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '328994371046632');
		fbq('track', 'PageView');
	</script>
	<script data-skip-moving="true">
	<?if ($curPage=="/basket/"){?>
		fbq('track', 'InitiateCheckout');
	<?}?>
	<?$APPLICATION->ShowViewContent("fbq_product")//отметка, что это товар?>
	</script>
	<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=328994371046632&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<div class="top-line">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"top", 
					array(
						"ROOT_MENU_TYPE" => "top",
						"CHILD_MENU_TYPE" => "",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"USE_EXT" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "",
						"DELAY" => "N"
					),
					false
				);
				?>
			</div>
			<div class="col-md-5 text-right">
				<div class="top-address"><?$APPLICATION->IncludeFile(SITE_DIR."include/address.php", Array(), Array("MODE"=>"html") );?></div>
			</div>
		</div>
	</div>
</div>

<div class="container head">
	<div class="row">
		<div class="col-md-3 col-xl-6">
			<a href="/" class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo.png" alt=""></a>
		</div>
		<div class="col-md-5 col-xl-3 align-self-center">
			<div class="worktime"><?$APPLICATION->IncludeFile(SITE_DIR."include/worktime.php", Array(), Array("MODE"=>"html") );?></div>
		</div>
		<div class="col-md-4 col-xl-3 text-right">
			<div class="phone"><?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("MODE"=>"html") );?></div>
			<a class="link-callback" data-fancybox="" data-touch="false" data-src="#callback" href="javascript:;" onclick="yaCounter50565871.reachGoal('phone'); return true;">Перезвоните мне</a>
		</div>
	</div>
</div>

<div class="nav-holder">
	<div class="container">
		<div class="row">
			<div class="col-lg-11 col-md-11">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"main", 
					array(
						"ROOT_MENU_TYPE" => "catalog",
						"CHILD_MENU_TYPE" => "",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "36000000",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "2",
						"USE_EXT" => "Y",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "",
						"DELAY" => "N"
					),
					false
				);
				?>
			</div>
			<div class="col-lg-1 col-md-1">
				<a class="head-search" href="/search/"></a>
			</div>
		</div>
	</div>
</div><!-- /.nav-holder -->

<div class="menu-toggle d-block d-md-none">
	<span></span>
	<span></span>
	<span></span>
</div>
<div class="menu-overlay"></div>
<div class="menu-mobile">
	<div class="menu-mobile-close">
		<i></i><i></i>
	</div>
	<div class="menu-flex-col">
		<a href="/" class="menu-mobile-logo"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo.png" alt=""></a>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"mobile", 
			array(
				"ROOT_MENU_TYPE" => "catalog",
				"CHILD_MENU_TYPE" => "",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "N",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"USE_EXT" => "Y",
				"ALLOW_MULTI_SELECT" => "N",
				"COMPONENT_TEMPLATE" => "",
				"DELAY" => "N"
			),
			false
		);
		?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"mobile", 
			array(
				"ROOT_MENU_TYPE" => "top",
				"CHILD_MENU_TYPE" => "",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "N",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "1",
				"USE_EXT" => "N",
				"ALLOW_MULTI_SELECT" => "N",
				"COMPONENT_TEMPLATE" => "",
				"DELAY" => "N"
			),
			false
		);
		?>
	</div>
	<div class="menu-flex-col">
		<div class="menu-mobile-phone"><?$APPLICATION->IncludeFile(SITE_DIR."include/phone.php", Array(), Array("MODE"=>"html") );?></div>
		<a class="btn btn-green" data-fancybox="" data-touch="false" data-src="#callback" href="javascript:;">Перезвоните мне</a>
	</div>
</div><!-- /.menu-mobile -->

<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "header-basket", Array(
	"HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
		"PATH_TO_BASKET" => SITE_DIR."basket/",	// Страница корзины
		"POSITION_FIXED" => "Y",	// Отображать корзину поверх шаблона
		"POSITION_HORIZONTAL" => "right",	// Положение по горизонтали
		"POSITION_VERTICAL" => "top",	// Положение по вертикали
		"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
		"SHOW_DELAY" => "N",
		"SHOW_EMPTY_VALUES" => "N",	// Выводить нулевые значения в пустой корзине
		"SHOW_IMAGE" => "Y",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
		"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
		"SHOW_PRICE" => "Y",
		"SHOW_PRODUCTS" => "N",	// Показывать список товаров
		"SHOW_SUMMARY" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
		"COMPONENT_TEMPLATE" => ".default",
		"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
		"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
		"MAX_IMAGE_SIZE" => "70",	// Максимальный размер картинки товара
	),
	false
);?>



	
<?if (!$isIndex){?>

	<?if ($hideH1Bg){?>
		<?$APPLICATION->AddBufferContent('ShowH1');?>
	<?} else {?>
		<?if(CSite::InDir('/catalog/')) {?>
			<div class="t-cover" style="background-image:url(<?$APPLICATION->ShowViewContent("cover_bg")?>);">
				<div class="container t-cover-container">
					<div class="t-cover-text text-center">
						<?$APPLICATION->AddBufferContent('ShowH1');?>
					</div>
				</div>
			</div>
		<?} else {?>
			<div class="t-cover t-cover-sm" style="background-image:url(<?=SITE_TEMPLATE_PATH?>/i/title-bg.jpg);">
				<div class="container t-cover-container">
					<div class="t-cover-text text-left">
						<?$APPLICATION->AddBufferContent('ShowH1');?>
					</div>
				</div>
			</div>
		<?}?>
	<?}?>

	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "crumbs", Array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-",
	),
	false
	);?>

	<div class="container container-content">
<?}?>	