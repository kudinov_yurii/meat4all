$(document).ready(function () {
	
	$('input[name="phone"], .field_phone').mask("+7 (999) 999-99-99");
	
	// при битриксовом аяксе
	$(function() {                                                          
		BX.addCustomEvent('onAjaxSuccess', function(){
			$('input[name="phone"], .field_phone').mask("+7 (999) 999-99-99");
		});    
	});

	$('.main-slider').slick({
		accessability: false,
		arrows: true,
		dots: false,
		autoplay: true,
		pauseOnHover: false,
		autoplaySpeed: 3000,
		speed: 1000,
		infinite:true,  
		fade: true,
		prevArrow: '<div class="slick-arrow arrow-left"><svg viewBox="0 0 23.9 43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <polyline fill="none" stroke="#f16a21" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="3" points="1.5,1.5 21.5,21.5 1.5,41.5"></polyline></svg></div>',
		nextArrow: '<div class="slick-arrow arrow-right"><svg viewBox="0 0 23.9 43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <polyline fill="none" stroke="#f16a21" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="3" points="1.5,1.5 21.5,21.5 1.5,41.5"></polyline></svg></div>',
	});
	
	$('.serts-slider').slick({
		accessability: false,
		arrows: true,
		dots: false,
		infinite:true,
		slidesToShow: 6,
		slidesToScroll: 6,
		prevArrow: '<div class="slick-arrow arrow-left"><svg viewBox="0 0 23.9 43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <polyline fill="none" stroke="#f16a21" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="3" points="1.5,1.5 21.5,21.5 1.5,41.5"></polyline></svg></div>',
		nextArrow: '<div class="slick-arrow arrow-right"><svg viewBox="0 0 23.9 43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <polyline fill="none" stroke="#f16a21" stroke-linejoin="butt" stroke-linecap="butt" stroke-width="3" points="1.5,1.5 21.5,21.5 1.5,41.5"></polyline></svg></div>',
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 575,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});
	
	$(".fixed-callback-btn").on('click', function() {
		$(this).toggleClass("_active");
		$(".fixed-callback-form").fadeToggle();
	});	
	
	$("[data-fancybox]").on('click', function() {
		$('.menu-mobile, .menu-overlay').removeClass("_opened");
		$('body').removeClass("hidden");
	});	
	
	
	$(".menu-toggle, .menu-mobile-close").on('click', function() {
		$('.menu-mobile, .menu-overlay').toggleClass("_opened");
		$('body').toggleClass("hidden");
	});
	
	// всплывающий баннер с формой
	/*setTimeout(function() {
		if(typeof $.cookie('popup_banner') == "undefined") {
			$.cookie('popup_banner', '1', { expires: 1, path: '/' });
			$.fancybox.open({
				src  : '#moving',
				type : 'inline',
			});
		}
	}, 30000);
	*/
	
	// Scroll top
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
					$('.scroll-top').fadeIn();
			} else {
					$('.scroll-top').fadeOut();
			}
	});
	$('.scroll-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
	
});

function initMap() {
	var map;
	var myLatLng = {lat: 55.600868, lng: 37.6093513};
	map = new google.maps.Map(document.getElementById('g-map'), {
			center: myLatLng,
			zoom: 14,
			draggable: true,
	});
	var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Россошанский проезд д.3',
	});
};