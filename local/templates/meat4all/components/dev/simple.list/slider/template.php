<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main-slider">
	<?
	foreach($arResult["ITEMS"] as $arItem){
		$arProps = $arItem["PROPERTIES"];	
		
		if ($arParams["IS_MOBILE"]===true && $arItem["PREVIEW_PICTURE"]["ID"]>0){
			$thumbImg = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>660, 'height'=>480), BX_RESIZE_IMAGE_PROPORTIONAL, false);
		} else {
			$thumbImg["src"] = $arItem["PREVIEW_PICTURE"]["SRC"];
		}
	?>
		<a class="item" <?if (!empty($arProps["LINK"]["VALUE"])){?>href="<?=$arProps["LINK"]["VALUE"]?>"<?}?> style="background-image: url('<?=$thumbImg["src"]?>');">
			<div class="container slide-container">
				<div class="slide-text">
					<div class="t-title"><?=$arItem["NAME"]?></div>
					<?if ($arItem["PREVIEW_TEXT"]){?>				
						<div class="slide-preview-text"><?=$arItem["PREVIEW_TEXT"]?></div>
					<?}?>
					<?if (!empty($arProps["SHOW_BTN"]["VALUE"])){?>
						<span class="btn btn-green btn-big"><?=($arProps["LINK"]["DESCRIPTION"]?:'Подробнее')?></span>
					<?}?>
				</div>
			</div>
		</a>
	<?}?>
</div>