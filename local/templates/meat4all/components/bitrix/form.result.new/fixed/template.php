<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
	<?if ($arResult["isFormNote"] != "Y"){?>
	
	<div class="fixed-callback-title"><?=$arResult["FORM_TITLE"]?></div>
	 <?=$arResult["FORM_HEADER"]?>
		
		<?if ($arResult["isFormErrors"] == "Y"):?>
			<?//=$arResult["FORM_ERRORS_TEXT"];?>
		<?endif;?>

			<?
				foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
				{
					if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
					{
						if ($FIELD_SID=="PAGE_URL"){
							$page_url = str_replace('value=""', 'value="'.$_SERVER["REQUEST_URI"].'"', $arQuestion["HTML_CODE"]);
							echo $page_url;
						} elseif ($FIELD_SID=="UTM"){
							global $FORM_UTM;
							$utm = str_replace('value=""', 'value="'.$FORM_UTM.'"', $arQuestion["HTML_CODE"]);
							echo $utm;
						} else {
							echo $arQuestion["HTML_CODE"];
						}
					}
					else
					{
						$class = "input field_".toLower($FIELD_SID);
						if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS']))
							$class .= ' input-error';
						
						$inputСaption=$arQuestion["CAPTION"]; 
						if ($arQuestion["REQUIRED"] == "Y")
							$inputСaption.=" *";
						$input = str_replace('name', 'placeholder="'.$inputСaption.'" name', $arQuestion["HTML_CODE"]);
						$input = str_replace('class="', 'class="'.$class.' ', $input);
				?>

						<?=$input?>	
				<?
					}
				} //endwhile
				?>
			
			<?if($arResult["isUseCaptcha"] == "Y"){?>
				<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
				<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
				<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
			<?}?>

			<input class="btn btn-green btn-big w-100" type="submit" value="<?=$arResult["arForm"]["BUTTON"]?>" name="web_form_submit">
			
			<div class="form-text-policy">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных и соглашаетесь с <a href="/policy/" target="_blank">политикой конфиденциальности</a></div>

		<?=$arResult["FORM_FOOTER"]?>


	<?} else {?>
		 <h2 class="text-center" style="margin-bottom:0;"><?=$arResult["FORM_NOTE"]?></h2>
		 <?if ($arResult["arForm"]["ID"]==2){?>
			<script>
				yaCounter50565871.reachGoal('sentvopros');
				console.log('reachGoal sentvopros');
			</script>
		<?}?>
	<?}?>