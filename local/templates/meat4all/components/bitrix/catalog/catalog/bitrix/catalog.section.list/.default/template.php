<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<?if (0 < $arResult["SECTIONS_COUNT"]){?>

	<?
	$textTop = GetSectionField($arParams["IBLOCK_ID"], $arResult["SECTION"]["ID"], "UF_TEXT_TOP");
	if ($textTop){?>	
		<div class="catalog-text-top"><?=$textTop?></div>
	<?}?>

	<div class="t-title t-title-cat">Категории раздела</div>
	
	<div class="row">
		<?foreach ($arResult['SECTIONS'] as &$arSection) {
			$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
			$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
			?>
			<div class="col-md-6" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
				<a class="category-item" href="<?=$arSection['SECTION_PAGE_URL'];?>">
					<?if ($arSection['PICTURE']['ID']){
						$thumbImg = CFile::ResizeImageGet($arSection['PICTURE']['ID'], array('width'=>600, 'height'=>400), BX_RESIZE_IMAGE_PROPORTIONAL, false);
					}?>
					<div class="category-item-cover" style="background-image:url(<?=$thumbImg["src"];?>);"></div>
					<div class="category-item-title"><?=$arSection['NAME'];?></div>
				</a>	
			</div>	
		<?}?>
	</div>
<?}?>