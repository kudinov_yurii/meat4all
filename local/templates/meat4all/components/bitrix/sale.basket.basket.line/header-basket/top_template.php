<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>

<?
if (!$compositeStub) {?>
	<a href="<?=$arParams['PATH_TO_BASKET'] ?>" class="header-basket <?if ($arResult['NUM_PRODUCTS']>0){?>_showed<?}?>">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"><path fill="none" stroke-width="2" stroke-miterlimit="10" d="M44 18h10v45H10V18h10z"></path><path fill="none" stroke-width="2" stroke-miterlimit="10" d="M22 24V11c0-5.523 4.477-10 10-10s10 4.477 10 10v13"></path></svg>
		<span class="basket-count"><?=$arResult['NUM_PRODUCTS']?></span>
		<?
		$priceBasket = explode("руб.", $arResult['TOTAL_PRICE']);
		?>
		<span class="basket-price">= <?=$priceBasket[0]?> ₽</span>
	</a>	
<?}?>