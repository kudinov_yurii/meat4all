<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="product-item">

	<a class="" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$imgTitle?>" data-entity="image-wrapper">
			
		<div class="product-item-img-wrap">
		
				<?
				if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
				{
					?>
					<div class="product-item-label-discount" id="<?=$itemIds['DSC_PERC']?>"
						<?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>>
						<span><?=-$price['PERCENT']?>%</span>
					</div>
					<?
				}
				if ($item['LABEL'])
				{
					?>
					<div id="<?=$itemIds['STICKER_ID']?>">
						<?
						if (!empty($item['LABEL_ARRAY_VALUE']))
						{
							foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
							{
								?>
								<div class="product-item-label-<?=$code?>"><?=$value?>
								</div>
								<?
							}
						}
						?>
					</div>
					<?
				}
				?>
		
			<img class="product-item-img" src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
		</div>	
			
		<div style="display:none;"><?//!!!?>
			<span class="product-item-image-slider-slide-container slide" id="<?=$itemIds['PICT_SLIDER']?>"
				<?=($showSlider ? '' : 'style="display: none;"')?>
				data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>" data-slider-wrap="true">
				<?
				if ($showSlider)
				{
					foreach ($morePhoto as $key => $photo)
					{
						?>
						<span class="product-item-image-slide item <?=($key == 0 ? 'active' : '')?>"
							style="background-image: url('<?=$photo['SRC']?>');">
						</span>
						<?
					}
				}
				?>
			</span>
			<span class="product-item-image-original" id="<?=$itemIds['PICT']?>"
				style="background-image: url('<?=$item['PREVIEW_PICTURE']['SRC']?>'); <?=($showSlider ? 'display: none;' : '')?>">
			</span>
			<div class="product-item-image-slider-control-container" id="<?=$itemIds['PICT_SLIDER']?>_indicator"
				<?=($showSlider ? '' : 'style="display: none;"')?>>
			</div>
		</div>
	</a>
	
	<a class="product-item-name" href="<?=$item['DETAIL_PAGE_URL']?>"><?=$productTitle?></a>

	
	<div class="product-item-price" data-entity="price-block" style="margin-top:15px;">
		<span class="product-item-price--current" id="<?=$itemIds['PRICE']?>">
			<?
			if (!empty($price))
			{
				if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
				{
					echo Loc::getMessage(
						'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
						array(
							'#PRICE#' => $price['PRINT_RATIO_PRICE'],
							'#VALUE#' => $measureRatio,
							'#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
						)
					);
				}
				else
				{
					echo $price['PRINT_RATIO_PRICE'];
				}
			}
			?>
		</span>
		<?
		if ($arParams['SHOW_OLD_PRICE'] === 'Y')
		{
			?>
			<span class="product-item-price-old" id="<?=$itemIds['PRICE_OLD']?>"
				<?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?>>
				<?=$price['PRINT_RATIO_BASE_PRICE']?>
			</span>
			<?
		}
		?>
	</div>

	<div class="product-item-hidden" data-entity="buttons-block">
		<?
		if (!$haveOffers)
		{
			if ($actualItem['CAN_BUY'])
			{
				?>
				<div id="<?=$itemIds['BASKET_ACTIONS']?>">
					<a class="btn btn-buy" id="<?=$itemIds['BUY_LINK']?>"
						href="javascript:void(0)" rel="nofollow">
						<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
					</a>
				</div>
				<?
			}
			else
			{
				?>
				<div>
					<a class="btn btn-link <?=$buttonSizeClass?>"
						id="<?=$itemIds['NOT_AVAILABLE_MESS']?>" href="javascript:void(0)" rel="nofollow">
						<?=$arParams['MESS_NOT_AVAILABLE']?>
					</a>
				</div>
				<?
			}
		}
		else
		{
				?>
				<div>
					<?
					?>
					<a class="btn btn-link <?=$buttonSizeClass?>"
						id="<?=$itemIds['NOT_AVAILABLE_MESS']?>" href="javascript:void(0)" rel="nofollow"
						<?=($actualItem['CAN_BUY'] ? 'style="display: none;"' : '')?>>
						<?=$arParams['MESS_NOT_AVAILABLE']?>
					</a>
					<div id="<?=$itemIds['BASKET_ACTIONS']?>" <?=($actualItem['CAN_BUY'] ? '' : 'style="display: none;"')?>>
						<a class="btn btn-buy" id="<?=$itemIds['BUY_LINK']?>"
							href="javascript:void(0)" rel="nofollow">
							<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
						</a>
					</div>
				</div>
				<?
		}
		?>
	</div>
	<?
	if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP']))
	{
		?>
		<div id="<?=$itemIds['PROP_DIV']?>">
			<?
			foreach ($arParams['SKU_PROPS'] as $skuProperty)
			{
				$propertyId = $skuProperty['ID'];
				$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
				if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
					continue;
				?>
				<div class="product-item-info-container product-item-hidden" data-entity="sku-block" style="display:none !important;">
					<div class="product-item-scu-container" data-entity="sku-line-block">
						<?=$skuProperty['NAME']?>
						<div class="product-item-scu-block">
							<div class="product-item-scu-list">
								<ul class="product-item-scu-item-list">
									<?
									foreach ($skuProperty['VALUES'] as $value)
									{
										if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
											continue;

										$value['NAME'] = htmlspecialcharsbx($value['NAME']);

										if ($skuProperty['SHOW_MODE'] === 'PICT')
										{
											?>
											<li class="product-item-scu-item-color-container" title="<?=$value['NAME']?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
												<div class="product-item-scu-item-color-block">
													<div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
														style="background-image: url('<?=$value['PICT']['SRC']?>');">
													</div>
												</div>
											</li>
											<?
										}
										else
										{
											?>
											<li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
												<div class="product-item-scu-item-text-block">
													<div class="product-item-scu-item-text"><?=$value['NAME']?></div>
												</div>
											</li>
											<?
										}
									}
									?>
								</ul>
								<div style="clear: both;"></div>
							</div>
						</div>
					</div>
				</div>
				<?
			}
			?>
		</div>
		<?
		foreach ($arParams['SKU_PROPS'] as $skuProperty)
		{
			if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
				continue;

			$skuProps[] = array(
				'ID' => $skuProperty['ID'],
				'SHOW_MODE' => $skuProperty['SHOW_MODE'],
				'VALUES' => $skuProperty['VALUES'],
				'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
			);
		}

		unset($skuProperty, $value);

		if ($item['OFFERS_PROPS_DISPLAY'])
		{
			foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
			{
				$strProps = '';

				if (!empty($jsOffer['DISPLAY_PROPERTIES']))
				{
					foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
					{
						$strProps .= '<dt>'.$displayProperty['NAME'].'</dt><dd>'
							.(is_array($displayProperty['VALUE'])
								? implode(' / ', $displayProperty['VALUE'])
								: $displayProperty['VALUE'])
							.'</dd>';
					}
				}

				$item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
			}
			unset($jsOffer, $strProps);
		}
	}


	?>
</div>