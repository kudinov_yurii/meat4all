<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach($arResult as $key => &$arItem){
  $nextKey = $key+1;
  $next = $arResult[$nextKey];
  $childKeys = array();
 
 
  while( $next['DEPTH_LEVEL'] > $arItem['DEPTH_LEVEL'] ){

    if($next['DEPTH_LEVEL'] == $arItem['DEPTH_LEVEL']+1 ){
      $childKeys []= $nextKey;
    }
	  $nextKey++;
	  $next = $arResult[$nextKey];
  }
  
  $arItem['CHILD_KEYS'] = $childKeys;
  
  $arItem["CLASS"] = "";
  if ($arItem["SELECTED"])
    $arItem["CLASS"].=' active';
}
?>