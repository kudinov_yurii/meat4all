<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
  <ul class="list-simple nav-main">
  <?
  foreach($arResult as $arItem){
    if( $arItem["DEPTH_LEVEL"] > 1) 
      continue;
		
		//$arLink = ( $arItem["CHILD_KEYS"]? 'javascript:void(0);' : $arItem["LINK"] );
    ?>
      <li>
        <a class="<?=$arItem["CLASS"]?>" href="<?=$arItem["LINK"]?>">
          <?=$arItem["TEXT"]?>
        </a>
        <?if($arItem["CHILD_KEYS"]){?>
          <ul class="list-simple sub-nav">
            <?foreach($arItem["CHILD_KEYS"] as $cKey){?>
            <?$subItem = $arResult[$cKey];?>
							<li>
								<a class="<?=$subItem["CLASS"]?>" href="<?=$subItem["LINK"]?>">
									<?=$subItem["TEXT"]?>
								</a> 
							</li>			
            <?}?>
          </ul>
        <?}?>
      </li>  
  <?}?>
  
  </ul>
<?}?>