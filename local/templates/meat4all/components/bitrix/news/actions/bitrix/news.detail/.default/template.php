<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
?>
<div class="actions-detail">

	<?if(is_array($arResult["DETAIL_PICTURE"])):?>
		<p><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"></p>
	<?endif?>

	<?=$arResult["DETAIL_TEXT"];?>

</div>

<? if ($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_ONE"]["LINK_ELEMENT_VALUE"]) { ?>
	<? foreach($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_ONE"]["LINK_ELEMENT_VALUE"] as $arElement) {
		$GLOBALS['actionSectionsFilter'] = "";
		$arrayLinksOne[] = $arElement["ID"];
	} ?>
	<? $GLOBALS['actionSectionsFilter'] = array('ID' => $arrayLinksOne);
		$APPLICATION->IncludeFile(SITE_DIR."include/action-section.php", 
		Array(
			"TYPE" => "GROUP_ONE",
			"TITLE" => $arResult["DISPLAY_PROPERTIES"]["TITLE_ONE"]["VALUE"],
			"SUBTITLE" => $arResult["DISPLAY_PROPERTIES"]["SUB_TEXT_ONE"]["VALUE"],
		), 
		Array("SHOW_BORDER"=>false));
		?>
<? } ?>

<? if ($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_TWO"]["LINK_ELEMENT_VALUE"]) { ?>
	<? foreach($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_TWO"]["LINK_ELEMENT_VALUE"] as $arElement) {
		$GLOBALS['actionSectionsFilter'] = "";
		$arrayLinksTwo[] = $arElement["ID"];
	} ?>
	<? $GLOBALS['actionSectionsFilter'] = array('ID' => $arrayLinksTwo);
		$APPLICATION->IncludeFile(SITE_DIR."include/action-section.php", 
		Array(
			"TYPE" => "GROUP_TWO",
			"TITLE" => $arResult["DISPLAY_PROPERTIES"]["TITLE_TWO"]["VALUE"],
			"SUBTITLE" => $arResult["DISPLAY_PROPERTIES"]["SUB_TEXT_TWO"]["VALUE"],
		), 
		Array("SHOW_BORDER"=>false));
		?>
<? } ?>

<? if ($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_THREE"]["LINK_ELEMENT_VALUE"]) { ?>
	<? foreach($arResult["DISPLAY_PROPERTIES"]["ACTION_GOODS_THREE"]["LINK_ELEMENT_VALUE"] as $arElement) {
		$GLOBALS['actionSectionsFilter'] = "";
		$arrayLinksThree[] = $arElement["ID"];
	} ?>
	<? $GLOBALS['actionSectionsFilter'] = array('ID' => $arrayLinksThree);
		$APPLICATION->IncludeFile(SITE_DIR."include/action-section.php", 
		Array(
			"TYPE" => "GROUP_THREE",
			"TITLE" => $arResult["DISPLAY_PROPERTIES"]["TITLE_THREE"]["VALUE"],
			"SUBTITLE" => $arResult["DISPLAY_PROPERTIES"]["SUB_TEXT_THREE"]["VALUE"],
		), 
		Array("SHOW_BORDER"=>false));
		?>
<? } ?>