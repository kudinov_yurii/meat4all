<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
	<div class="basket-checkout-container" data-entity="basket-checkout-aligner">
		<?
		if ($arParams['HIDE_COUPON'] !== 'Y')
		{
			?>
			<div class="basket-coupon-section">
				<div class="basket-coupon-block-field">
					<div class="basket-coupon-block-field-description">
						<?=Loc::getMessage('SBB_COUPON_ENTER')?>:
					</div>
					<div class="form">
						<div class="form-group" style="position: relative;">
							<input type="text" class="form-control" id="" placeholder="" data-entity="basket-coupon-input">
							<span class="basket-coupon-block-coupon-btn"></span>
						</div>
					</div>
				</div>
			</div>
			<?
		}
		?>
		<div class="text-right basket-info">

			<div class="basket-total-min-note" style="<?if ($arResult["allSum"]>=4000){?>display:none;<?}?>">Минимальный заказ: 4000 ₽</div>
			<?/* 
			{{#DISCOUNT_PRICE_FORMATED}}
				<div class="basket-coupon-block-total-price-old">
					{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
				</div>
			{{/DISCOUNT_PRICE_FORMATED}}
			 */?>
			<div class="basket-total">
				{{#DISCOUNT_PRICE_FORMATED}}
					<?=Loc::getMessage('SBB_TOTAL')?>: <span>{{{PRICE_WITHOUT_DISCOUNT_NEW}}}</span> ₽
					<div class="basket_price_whith_discont"><span class="title_span">Сумма со скидкой:</span> <span data-entity="basket-total-price">{{{PRICE}}}</span> ₽</div>
				{{/DISCOUNT_PRICE_FORMATED}}
				
				{{^DISCOUNT_PRICE_FORMATED}}
					<?=Loc::getMessage('SBB_TOTAL')?>: <span data-entity="basket-total-price">{{{PRICE}}}</span> ₽
				{{/DISCOUNT_PRICE_FORMATED}}
			</div>			
			<?/* 
			<br><button class="btn btn-lg btn-default basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
				data-entity="basket-checkout-button">
				<?=Loc::getMessage('SBB_ORDER')?>
			</button>
			 */?>
		</div>

		<?
		if ($arParams['HIDE_COUPON'] !== 'Y')
		{
		?>
			<div class="basket-coupon-alert-section">
				<div class="basket-coupon-alert-inner">
					{{#COUPON_LIST}}
					<div class="basket-coupon-alert text-{{CLASS}}">
						<span class="basket-coupon-text">
							<strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
							{{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}}
						</span>
						<span class="close-link btn" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
							<?=Loc::getMessage('SBB_DELETE')?>
						</span>
					</div>
					{{/COUPON_LIST}}
				</div>
			</div>
			<?
		}
		?>
	</div>
</script>