<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

?>
<script id="basket-item-template" type="text/html">
	<tr class="basket-items-list-item-container" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
		{{^SHOW_RESTORE}}
			<td>
					{{#DETAIL_PAGE_URL}}
						<a href="{{DETAIL_PAGE_URL}}" class="basket-item-image-link">
					{{/DETAIL_PAGE_URL}}

					<img class="b-product-img" alt="{{NAME}}"
						src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}">

					{{#DETAIL_PAGE_URL}}
						</a>
					{{/DETAIL_PAGE_URL}}
			</td>
			
			<td>
				{{#IS_GIFT}}
					<span class="basket-label-gift">Подарок</span>
				{{/IS_GIFT}}
				<a href="{{DETAIL_PAGE_URL}}" class="b-product-title">
					<span data-entity="basket-item-name">{{NAME}}</span>
				</a>
				
				{{#COLUMN_LIST}}
					{{!NAME}}
					<div data-column-property-code="{{CODE}}" data-entity="basket-item-property-column-value">
							Желаемый вес: <b>{{VALUE}}</b>
					</div>
				{{/COLUMN_LIST}}
				
				Итоговая стоимость будет определена после согласования с оператором, либо мы свяжемся сами для уточнения деталей заказа
				
			</td>
			
			<td class="b-product-quantity">
				{{^IS_GIFT}}
					<div class="{{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}"
						data-entity="basket-item-quantity-block">
						<span class="b-product-minus" data-entity="basket-item-quantity-minus"></span>
							<input type="text" class="b-product-qty" value="{{QUANTITY}}"
								{{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
								data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
								id="basket-item-quantity-{{ID}}">
						<span class="b-product-plus" data-entity="basket-item-quantity-plus"></span>
					</div>
				{{/IS_GIFT}}
			</td>
			
			<?
			if ($usePriceInAdditionalColumn)
			{
				?>
				<td class="b-product-cost">
					{{#SHOW_DISCOUNT_PRICE}}
						<div class="basket-item-price-old">
							<span class="basket-item-price-old-text">
								{{{FULL_PRICE_FORMATED}}}
							</span>
						</div>
					{{/SHOW_DISCOUNT_PRICE}}

					<span id="basket-item-price-{{ID}}">
						{{{!PRICE_FORMATED}}} {{{PRICE}}} ₽
					</span>
				</td>
				<?
			}
			?>
			<?
			/*if ($useSumColumn)
			{
				?>
				<td class="basket-items-list-item-price<?=(!isset($mobileColumns['SUM']) ? ' hidden-xs' : '')?>">
					<div class="basket-item-block-price">
						{{#SHOW_DISCOUNT_PRICE}}
							<div class="basket-item-price-old">
								<span class="basket-item-price-old-text" id="basket-item-sum-price-old-{{ID}}">
									{{{SUM_FULL_PRICE_FORMATED}}}
								</span>
							</div>
						{{/SHOW_DISCOUNT_PRICE}}

						<div class="basket-item-price-current">
							<span class="basket-item-price-current-text" id="basket-item-sum-price-{{ID}}">
								{{{SUM_PRICE_FORMATED}}}
							</span>
						</div>

						{{#SHOW_DISCOUNT_PRICE}}
							<div class="basket-item-price-difference">
								<?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
								<span id="basket-item-sum-price-difference-{{ID}}" style="white-space: nowrap;">
									{{{SUM_DISCOUNT_PRICE_FORMATED}}}
								</span>
							</div>
						{{/SHOW_DISCOUNT_PRICE}}
						{{#SHOW_LOADING}}
							<div class="basket-items-list-item-overlay"></div>
						{{/SHOW_LOADING}}
					</div>
				</td>
				<?
			} */

			if ($useActionColumn)
			{
				?>
				<td class="b-td-remove">
					<span class="b-product-remove" data-entity="basket-item-delete" title="Удалить"></span>
				</td>
				<?
			}
			?>
		{{/SHOW_RESTORE}}
	</tr>
</script>