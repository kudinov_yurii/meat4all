ymaps.ready(function() 
{	
    var map,        
        
		center = [37.630229, 55.622763],

        zoom = 10;

    map = new ymaps.Map('yamap', {
        center: center,
        zoom: zoom,
        controls: [
			'zoomControl'
		]
    });
	
	var regionName = {
			obj1: {loc: 'Москва, район Северное Бутово', color: '#007bff'},
			obj2: {loc: 'Москва, район Южное бутово', color: '#F08080'},
			obj3: {loc: 'Москва, район Южное Чертаново', color: '#FFEFD5'},			
			obj4: {loc: 'Москва, район Чертаново Северное', color: '#F0E68C'},
			obj5: {loc: 'Москва, район Центральное Чертаново', color: '#FFD700'},
			obj6: {loc: 'Москва, район Ясенево', color: '#DDA0DD'},
			obj7: {loc: 'Москва, район Тёплый Стан', color: '#4B0082'}
		};


    var url = "https://nominatim.openstreetmap.org/search";
	

	$.getJSON(url, {q: regionName['obj1']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p1 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj1']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj1']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p1);
					
				}
			});

		}, function (err) {
			console.log(err);
		});	

	$.getJSON(url, {q: regionName['obj2']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p2 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj2']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj2']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p2);
					
				}
			});

		}, function (err) {
			console.log(err);
	});	

	$.getJSON(url, {q: regionName['obj3']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p3 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj3']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj3']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p3);
					
				}
			});

		}, function (err) {
			console.log(err);
		});			
	
	$.getJSON(url, {q: regionName['obj4']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p4 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj4']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj4']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p4);
					
				}
			});

		}, function (err) {
			console.log(err);
		});	
	
	$.getJSON(url, {q: regionName['obj5']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p5 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj5']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj5']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p5);
					
				}
			});

		}, function (err) {
			console.log(err);
		});		
	
	$.getJSON(url, {q: regionName['obj6']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p6 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj6']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj6']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p6);
					
				}
			});

		}, function (err) {
			console.log(err);
		});


	$.getJSON(url, {q: regionName['obj7']['loc'], format: "json", polygon_geojson: 1})
		.then(function (data) {
			
			$.each(data, function(ix, place) {
				
				if ("relation" == place.osm_type) {
					
					var p7 = new ymaps.Polygon(
						place.geojson.coordinates, 
						{
							hintContent: "<div style='font-size:16px;padding:30px;'>" + regionName['obj7']['loc'] +"</div>"
						},
						{
							fillColor: regionName['obj7']['color'],
							opacity: 0.6,			
							strokeWidth: 1		
						}
					);						
					map.geoObjects.add(p7);
					
				}
			});

		}, function (err) {
			console.log(err);
		});		

});
/*
$(document).ready(function(){
	$(".show_delivery_map").on('click', function(){
		$('.yamap_wrp').slideToggle();
		return false;
	});
})
*/