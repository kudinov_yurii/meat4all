<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заказ оформлен");

$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$getWebForm= htmlspecialchars($request->getQuery("WEB_FORM_ID")); 
$getResultID= htmlspecialchars($request->getQuery("RESULT_ID")); 
$getFormResult= htmlspecialchars($request->getQuery("formresult")); 
?>

<p style="font-size:22px; text-align:center">
Спасибо, Ваш заказ оформлен.<br>
Для определения даты и время доставки заказа с Вами свяжется оператор.<br>
<a href="/">Перейти на главную страницу сайта</a>
</p>
<?if ( preg_match("/basket/", $_SERVER["HTTP_REFERER"]) && $getWebForm==3 && $getFormResult=='addok' && $getResultID>0){?>
	<script>
	window.onload = function() {
		// передаем в пиксель сумму заказа из хранилища и очищаем его
		if ( sessionStorage.getItem("basketSum")!==null ){
				fbq('track', 'Purchase', {
					value: sessionStorage.getItem("basketSum"),
					currency: 'RUB',
					content_ids: 'price',
					content_type: 'meat',
				});
				sessionStorage.removeItem("basketSum");
		}
		
		yaCounter50565871.reachGoal('sentzakaz');
		console.log('sentzakaz');
	}
	</script>
<?}?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>