<?$detect = new Mobile_Detect;?>
<?$APPLICATION->IncludeComponent("dev:simple.list", "slider", Array(
	"IS_MOBILE" => ($detect->isMobile()?true:false),
	"COUNT" => "30",	// Количество элементов на странице
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "4",	// Код информационного блока
		"IBLOCK_TYPE" => "-",	// Тип информационного блока (используется только для проверки)
		"PARENT_SECTION" => "",	// ID раздела
		"PROPERTY_CODE" => array(	// Свойства
			0 => "LINK",
			1 => "",
		),
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"COMPONENT_TEMPLATE" => ""
	),
	false
);?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index-section.php", 
Array(
	"TYPE" => "POPULAR",
	"TITLE" => "Популярные товары: самое актуальное и востребованное",
	"SUBTITLE" => "Посмотрите топ продаж — самые популярные и вкусные позиции",
), 
Array("SHOW_BORDER"=>false));
?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index-section.php", 
Array(
	"TYPE" => "NEW",
	"TITLE" => "Внимание: обновления линейки продукции",
	"SUBTITLE" => "Попробуйте деликатесы первыми. Заказывайте новинки по приятным ценам",
), 
Array("SHOW_BORDER"=>false));
?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index-section.php", 
Array(
	"TYPE" => "ACTIONS",
	"TITLE" => "Акции и скидки: с нами выгодно",
	"SUBTITLE" => "Ознакомьтесь с горящими предложениями",
), 
Array("SHOW_BORDER"=>false));
?>

<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-how.php", Array(), Array("MODE"=>"html") );?>


<div class="section-benefits">
	<div class="container">
		<div class="h1 text-center section-title">
			<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-benefits-title.php", Array(), Array("MODE"=>"html") );?>
		</div>
		<div class="section-text">
			<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-benefits-text.php", Array(), Array("MODE"=>"html") );?>
		</div>
	
		<?$APPLICATION->IncludeComponent("dev:simple.list", "pluses", Array(
			"COUNT" => "30",	// Количество элементов на странице
				"FIELD_CODE" => array(	// Поля
					0 => "",
					1 => "",
				),
				"IBLOCK_ID" => "6",	// Код информационного блока
				"IBLOCK_TYPE" => "-",	// Тип информационного блока (используется только для проверки)
				"PARENT_SECTION" => "",	// ID раздела
				"PROPERTY_CODE" => array(	// Свойства
					0 => "",
					1 => "",
				),
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
				"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
				"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
				"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
				"COMPONENT_TEMPLATE" => ""
			),
			false
		);?>		
		
	</div>
</div><!-- /.section-benefits -->


<div class="section-bottom container">
	<div class="row no-gutters">
		<div class="col-md-7">
			<div class="section-title">
				<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-bottom-title.php", Array(), Array("MODE"=>"html") );?>
			</div>
			<div class="section-text">
			  <h1 style="display:none;">Поставщик мяса в Москве оптом и в розницу с доставкой</h1>
				<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-bottom-text.php", Array(), Array("MODE"=>"html") );?>
			</div>
		</div>
		<div class="col-md-5 col-sm-8 col-8">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/pork.png">
		</div>
	</div>
	<div class="line"></div>
</div><!-- /.section-bottom -->
<?$APPLICATION->IncludeFile(SITE_DIR."include/index/section-bottom.php", Array(), Array("MODE"=>"html") );?>
