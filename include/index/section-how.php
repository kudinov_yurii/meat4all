<div class="section-how">
	<div class="container">
		<div class="h1 text-center section-title">Как сделать заказ</div>

		<div class="row">
			<div class="col-md-4 how-item">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/how-1.svg">
				<div class="how-item-name">Совершите покупку</div>
				<div class="how-item-text">Оформите заказ на сайте<br> или через оператора</div>
			</div>
			<div class="col-md-4 how-item">
				<div class="how-item-dots"><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i></div>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/how-2.svg">
				<div class="how-item-name">Дождитесь подтверждения</div>
				<div class="how-item-text">Оператор перезвонит для согласования удобного времени доставки и уточнения стоимости заказа (вес мяса варьируется). Доставляем на следующий день после поступления заявки.</div>
			</div>
			<div class="col-md-4 how-item">
				<div class="how-item-dots"><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i></div>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/how-3.svg">
				<div class="how-item-name">Получите товар в удобное вам время</div>
				<div class="how-item-text">Курьер справится с доставкой мяса даже в час пик по Москве. Предварительно он сообщит вам, что выезжает.</div>
			</div>
		</div>
	</div>
</div><!-- /.section-how -->